# == Class: profile::common::users
# Common users profile
#
# === Parameters
#
# None
#
# === Variables
#
# None
#
# === Examples
#
#  include profile::common::users
#
# === Authors
#
# Rodney Brown <rodney@punchtech.com>
#
# === Copyright
#
# Copyright 2015 Rodney Brown
#
class profile::common::users {
  users { 'common': }
}
